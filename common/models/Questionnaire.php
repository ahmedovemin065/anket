<?php

namespace common\models;

use Yii;


/**
 * This is the model class for table "questionnaire".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $region
 * @property string $city
 * @property int $rate
 * @property string $comment
 * @property string $created_at
 */
class Questionnaire extends \yii\db\ActiveRecord
{
    public $dts;
    public $cnt;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questionnaire';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'region', 'city', 'rate', 'gender','comment'], 'required'],
            [['rate', 'gender'], 'integer'],
            [['comment'], 'string'],
            [['created_at'], 'safe'],
            [['name', 'email', 'phone', 'region', 'city'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'region' => 'Region',
            'city' => 'City',
            'rate' => 'Rate',
            'gender' => 'Gender',
            'comment' => 'Comment',
            'created_at' => 'Created At',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {

            $this->created_at = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }
}
