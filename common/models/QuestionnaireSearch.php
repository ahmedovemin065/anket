<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Questionnaire;
use kartik\daterange\DateRangeBehavior;
/**
 * QuestionnaireSearch represents the model behind the search form of `common\models\Questionnaire`.
 */
class QuestionnaireSearch extends Questionnaire
{
    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;


    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'rate', 'gender'], 'integer'],
            [['name', 'email', 'phone', 'region', 'city', 'comment', 'created_at'], 'safe'],
            [['createTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Questionnaire::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rate' => $this->rate,
            'gender' => $this->gender,
            //'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        $query->andFilterWhere(['>=', 'UNIX_TIMESTAMP(created_at)', $this->createTimeStart])
            ->andFilterWhere(['<', 'UNIX_TIMESTAMP(created_at)', $this->createTimeEnd]);

        //$a = $query->createCommand()->getRawSql();
        //die($a);

        return $dataProvider;
    }

    // I can merge the above function with the below function :) for less code
    public function searchForChart($params)
    {
        $query = Questionnaire::find();

        $query->select("COUNT(id) AS cnt, DATE(created_at) AS dts");
        $query->groupBy(['dts']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'gender' => $this->gender,
            //'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'city', $this->city]);

        $query->andFilterWhere(['>=', 'UNIX_TIMESTAMP(created_at)', $this->createTimeStart])
            ->andFilterWhere(['<', 'UNIX_TIMESTAMP(created_at)', $this->createTimeEnd]);


        return $dataProvider;
    }
}
