<?php

namespace backend\controllers;

use common\models\LoginForm;
use common\models\Questionnaire;
use common\models\QuestionnaireSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new QuestionnaireSearch();
        $dataProvider = $searchModel->searchForChart($this->request->queryParams);

        //$days = Yii::$app->db->createCommand("SELECT DATE(created_at) AS dates FROM `questionnaire` GROUP BY dates")->queryColumn();
        //$men  = Yii::$app->db->createCommand("SELECT count(id) FROM `questionnaire` WHERE gender = 1 GROUP BY DATE(created_at)")->queryColumn();
        //$women = Yii::$app->db->createCommand("SELECT count(id) FROM `questionnaire` WHERE gender = 0 GROUP BY DATE(created_at)")->queryColumn();

        //var_dump($men);

        return $this->render('index', [
            //'days' => $days,
            //'men' => array_map('intval', $men),
            //'women' =>array_map('intval', $women),
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Login action.
     *
     * @return string|Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'blank';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
