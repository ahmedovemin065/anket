<?php

/* @var $this yii\web\View */
//use miloschuman\highcharts\Highcharts;
use sjaakp\gcharts\ColumnChart;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>


    </div>

    <div class="body-content">
        <div class="row">
           <!-- <div class="col-lg-12 hide">

                <?php
/*                echo Highcharts::widget([
                    'options' => [
                        'title' => ['text' => 'Questionnaire'],

                        'xAxis' => [
                            'categories' => $days
                        ],
//                        'yAxis' => [
//                            'title' => ['text' => 'Questiona']
//                        ],
                        'series' => [
                            ['name' => 'Men', 'data' => $men],
                            ['name' => 'women', 'data' => $women]
                        ]
                    ]
                ]);
                */?>

            </div>-->

            <div class="col-lg-9">
                <?= ColumnChart::widget([
                    'height' => '400px',
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'dts:string',  // first column: domain
                        'cnt',
                        [               // third column: tooltip
                            'value' => function($model, $a, $i, $w) {
                                return "Count: ".$model->cnt;
                            },
                            'type' => 'string',
                            'role' => 'tooltip',
                        ],
                    ],
                    'options' => [
                        'title' => 'Statistic Chart',
                    ],
                ]) ?>
            </div>
            <div class="coll-lg-3">

                <div class="questionnaire-search">

                    <?php $form = ActiveForm::begin([
                        'action' => ['index'],
                        'method' => 'get',
                        'class'=> 'form-inline'
                    ]); ?>
                    <?= $form->field($model, 'region') ?>

                    <?=$form->field($model, 'city') ?>

                    <?=$form->field($model, 'rate')->dropDownList(['1' => 'Male', '0' => 'Female'], [ 'prompt'=>'---']) ?>

                    <div class="form-group">
                        <label for=""><?=$model->getAttributeLabel('created_at')?></label>
                        <?php
                        echo DateRangePicker::widget([
                            'model'=>$model,
                            'attribute'=>'createTimeRange',
                            'convertFormat'=>true,
                            'pluginOptions'=>[
                                'timePicker'=>false,
                                'timePickerIncrement'=>30,
                                'locale'=>[
                                    'format'=>'Y-m-d h:i:s'//
                                ]
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>

            </div>
        </div>

    </div>
</div>
