<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model common\models\QuestionnaireSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questionnaire-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'class'=> 'form-inline'
    ]); ?>
    <?= $form->field($model, 'region') ?>

    <?=$form->field($model, 'city') ?>

    <?=$form->field($model, 'rate')->dropDownList(['1' => 'Male', '0' => 'Female'], [ 'prompt'=>'---']) ?>

    <div class="form-group">
        <label for=""><?=$model->getAttributeLabel('created_at')?></label>
    <?php
    echo DateRangePicker::widget([
        'model'=>$model,
        'attribute'=>'createTimeRange',
        'convertFormat'=>true,
        'pluginOptions'=>[
            'timePicker'=>false,
            'timePickerIncrement'=>30,
            'locale'=>[
                'format'=>'Y-m-d h:i:s'//
            ]
        ]
    ]);
    ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
