<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\QuestionnaireSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Questionnaires';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questionnaire-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'name',
            'email:email',
            'phone',
            'region',
            'city',
            //'gender',
            [
                'attribute' => 'gender',
                'filter'    => Html::activeDropDownList($searchModel, 'gender', ['1' => 'Male', '0' => 'Female'], ['class'=>'form-control', 'prompt'=>'---']),
            ],
            'rate',
            'comment:ntext',
            //'created_at',
            [
                    'attribute' => 'created_at',
                    'filter'=>DateRangePicker::widget([
                                                        'model'=>$searchModel,
                                                        'attribute'=>'createTimeRange',
                                                        'convertFormat'=>true,
                                                        'pluginOptions'=>[
                                                            'timePicker'=>false,
                                                            'timePickerIncrement'=>30,
                                                            'locale'=>[
                                                                'format'=>'Y-m-d h:i:s'//
                                                            ]
                                                        ]
                                                    ])
            ],

            [
                'class' => ActionColumn::className(),
                'template'=>'{view} {delete}',
                'urlCreator' => function ($action, \common\models\Questionnaire $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
