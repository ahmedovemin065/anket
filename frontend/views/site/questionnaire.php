<?php
/**
 * Created by PhpStorm.
 * User: Emin
 * Date: 1/13/2022
 * Time: 11:28 AM
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><?=Yii::$app->name?></h1>
        <p class="lead">We appreciate your point of view</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">

                <div class="questionnaire-form">

                    <?php $form = ActiveForm::begin();?>

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'gender')->radioList(['1' => 'Male', '0' => 'Female']) ?>

                    <?= $form->field($model, 'rate')->dropDownList([0,1,2,3,4,5,6,7,8,9,10]) ?>

                    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>

        </div>

    </div>
</div>


