<?php

use yii\db\Migration;

/**
 * Class m220112_144228_anket
 */
class m220112_144228_anket extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%questionnaire}}', [
            'id'        => $this->primaryKey(),
            'name'      => $this->string()->notNull(),
            'email'     => $this->string()->notNull(),
            'phone'     => $this->string()->notNull(),
            'region'    => $this->string()->notNull(),
            'city'      => $this->string()->notNull(),
            'rate'      => $this->tinyInteger(2)->notNull(),
            'gender'    => $this->tinyInteger(1)->notNull(),
            'comment'   => $this->text()->notNull(),
            //'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->dateTime()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220112_144228_anket cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220112_144228_anket cannot be reverted.\n";

        return false;
    }
    */
}
